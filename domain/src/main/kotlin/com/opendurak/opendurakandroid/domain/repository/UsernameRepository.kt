package com.opendurak.opendurakandroid.domain.repository

interface UsernameRepository {

    suspend fun getUsername(): String

    suspend fun setUsername(username: String)

}