package com.opendurak.opendurakandroid.domain.interactor

import com.opendurak.opendurakandroid.domain.repository.GameCredentialsRepository
import com.opendurak.opendurakandroid.domain.repository.GameRepository

class StartGameUseCase(
    private val gameRepository: GameRepository,
    private val gameCredentialsRepository: GameCredentialsRepository,
) {

    suspend fun execute() {
        val gameCredentials = gameCredentialsRepository.getSavedGameCredentials()!!
        gameRepository.start(gameCredentials)
    }
}