package com.opendurak.opendurakandroid.domain.interactor

import com.opendurak.opendurakandroid.domain.repository.GameCredentialsRepository
import com.opendurak.opendurakandroid.domain.repository.GameRepository
import com.opendurak.opendurakapi.Card

class AttackUseCase(
    private val gameRepository: GameRepository,
    private val gameCredentialsRepository: GameCredentialsRepository,
) {

    suspend fun execute(card: Card) {
        val gameCredentials = gameCredentialsRepository.getSavedGameCredentials()!!
        gameRepository.attack(card, gameCredentials)
    }
}