package com.opendurak.opendurakandroid.domain.repository

import com.opendurak.opendurakandroid.domain.model.GameCredentials

interface GameCredentialsRepository {

    suspend fun saveGameCredentials(gameCredentials: GameCredentials)

    suspend fun getSavedGameCredentials(): GameCredentials?

    suspend fun clearSavedGameCredentials()
}