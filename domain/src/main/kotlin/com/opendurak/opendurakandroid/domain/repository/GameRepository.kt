package com.opendurak.opendurakandroid.domain.repository

import com.opendurak.opendurakandroid.domain.model.GameCredentials
import com.opendurak.opendurakapi.Card
import com.opendurak.opendurakapi.DefendRequest
import com.opendurak.opendurakapi.GameState
import kotlinx.coroutines.flow.Flow

interface GameRepository {

    suspend fun getUpdates(gameCredentials: GameCredentials): Flow<GameState?>

    suspend fun start(gameCredentials: GameCredentials)

    suspend fun attack(card: Card, gameCredentials: GameCredentials)

    suspend fun defend(defendRequest: DefendRequest, gameCredentials: GameCredentials)

    suspend fun help(card: Card, gameCredentials: GameCredentials)

    suspend fun giveUp(gameCredentials: GameCredentials)
}