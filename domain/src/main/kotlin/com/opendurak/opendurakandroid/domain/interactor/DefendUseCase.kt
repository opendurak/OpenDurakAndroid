package com.opendurak.opendurakandroid.domain.interactor

import com.opendurak.opendurakandroid.domain.repository.GameCredentialsRepository
import com.opendurak.opendurakandroid.domain.repository.GameRepository
import com.opendurak.opendurakapi.DefendRequest

class DefendUseCase(
    private val gameRepository: GameRepository,
    private val gameCredentialsRepository: GameCredentialsRepository,
) {

    suspend fun execute(defendRequest: DefendRequest) {
        val gameCredentials = gameCredentialsRepository.getSavedGameCredentials()!!
        gameRepository.defend(defendRequest, gameCredentials)
    }
}