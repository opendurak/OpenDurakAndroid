package com.opendurak.opendurakandroid.domain.repository

import com.opendurak.opendurakandroid.domain.model.GameCredentials
import kotlinx.coroutines.flow.Flow

interface LobbiesRepository {

    suspend fun getLobbies(): List<String>

    suspend fun listenToLobby(gameCredentials: GameCredentials): Flow<List<String>>

    suspend fun createLobby(): String

    suspend fun joinLobby(userName: String, lobbyId: String): String
}