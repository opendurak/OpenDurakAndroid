package com.opendurak.opendurakandroid.domain.interactor

import com.opendurak.opendurakandroid.domain.model.GameCredentials
import com.opendurak.opendurakandroid.domain.repository.GameCredentialsRepository
import com.opendurak.opendurakandroid.domain.repository.GameRepository
import com.opendurak.opendurakandroid.domain.repository.LobbiesRepository
import com.opendurak.opendurakandroid.domain.repository.UsernameRepository
import com.opendurak.opendurakapi.GameState
import kotlinx.coroutines.flow.Flow

class ConnectLobbyUseCase(
    private val lobbiesRepository: LobbiesRepository,
    private val gameRepository: GameRepository,
    private val gameCredentialsRepository: GameCredentialsRepository,
    private val usernameRepository: UsernameRepository,
) {

    suspend fun joinAndConnect(
        lobbyId: String,
    ): Pair<Flow<List<String>>, Flow<GameState?>> {
        val username = usernameRepository.getUsername()
        val userId = lobbiesRepository.joinLobby(username, lobbyId)
        val gameCredentials = GameCredentials(lobbyId, userId)
        gameCredentialsRepository.saveGameCredentials(gameCredentials)

        return Pair(
            lobbiesRepository.listenToLobby(gameCredentials),
            gameRepository.getUpdates(gameCredentials)
        )
    }

    suspend fun getJoinedLobbyId(): String? {
        return gameCredentialsRepository.getSavedGameCredentials()?.lobbyId
    }

    suspend fun leaveLobby() {
        gameCredentialsRepository.clearSavedGameCredentials()
    }

    @Throws(IllegalStateException::class)
    suspend fun connect(): Pair<Flow<List<String>>, Flow<GameState?>> {
        val gameCredentials = gameCredentialsRepository.getSavedGameCredentials()
            ?: throw IllegalStateException("Not joined a lobby")

        return Pair(
            lobbiesRepository.listenToLobby(gameCredentials),
            gameRepository.getUpdates(gameCredentials)
        )
    }
}