package com.opendurak.opendurakandroid.domain.model

data class GameCredentials(
    val lobbyId: String,
    val userId: String,
)