package com.opendurak.opendurakandroid.domain.interactor

import com.opendurak.opendurakandroid.domain.repository.LobbiesRepository

class GetLobbiesUseCase(
    private val lobbiesRepository: LobbiesRepository,
) {

    suspend fun execute() = lobbiesRepository.getLobbies()
}