package com.opendurak.opendurakandroid.domain.interactor

import com.opendurak.opendurakandroid.domain.repository.LobbiesRepository

class CreateLobbyUseCase(
    private val lobbiesRepository: LobbiesRepository,
) {

    suspend fun execute() {
        lobbiesRepository.createLobby()
    }
}