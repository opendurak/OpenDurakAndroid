package com.opendurak.opendurakandroid.domain.interactor

import com.opendurak.opendurakandroid.domain.repository.UsernameRepository

class UsernameUseCase(
    private val usernameRepository: UsernameRepository,
) {

    suspend fun getUsername(): String = usernameRepository.getUsername()

    suspend fun setUsername(username: String) = usernameRepository.setUsername(username)

    fun isUsernameValid(username: String) = username.trim().length >= 3
}