package com.opendurak.opendurakandroid.domain.interactor

import com.opendurak.opendurakandroid.domain.repository.GameCredentialsRepository
import com.opendurak.opendurakandroid.domain.repository.GameRepository

class GiveUpUseCase(
    private val gameRepository: GameRepository,
    private val gameCredentialsRepository: GameCredentialsRepository,
) {

    suspend fun execute() {
        val gameCredentials = gameCredentialsRepository.getSavedGameCredentials()!!
        gameRepository.giveUp(gameCredentials)
    }
}