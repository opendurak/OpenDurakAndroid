package com.opendurak.opendurakandroid.domain.interactor

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakandroid.domain.model.GameCredentials
import com.opendurak.opendurakandroid.domain.repository.GameCredentialsRepository
import com.opendurak.opendurakandroid.domain.repository.GameRepository
import com.opendurak.opendurakandroid.domain.repository.LobbiesRepository
import com.opendurak.opendurakandroid.domain.repository.UsernameRepository
import com.opendurak.opendurakapi.GameState
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.runBlocking
import org.junit.Test

class ConnectLobbyUseCaseTest {
    private val lobbiesRepository = mockk<LobbiesRepository>(relaxed = true)
    private val gameRepository = mockk<GameRepository>(relaxed = true)
    private val gameCredentialsRepository = mockk<GameCredentialsRepository>(relaxed = true)
    private val usernameRepository = mockk<UsernameRepository>(relaxed = true)
    private val connectLobbyUseCase = ConnectLobbyUseCase(
        lobbiesRepository, gameRepository, gameCredentialsRepository, usernameRepository
    )

    @Test
    fun `joinAndConnect should joinLobby`() {
        coEvery { usernameRepository.getUsername() } returns "u"
        runBlocking { connectLobbyUseCase.joinAndConnect("l") }
        coVerify(exactly = 1) { lobbiesRepository.joinLobby("u", "l") }
    }

    @Test
    fun `joinAndConnect should saveCredentials`() {
        coEvery { lobbiesRepository.joinLobby(any(), any()) } returns "id"
        runBlocking { connectLobbyUseCase.joinAndConnect("l") }
        coVerify(exactly = 1) {
            gameCredentialsRepository.saveGameCredentials(GameCredentials("l", "id"))
        }
    }

    @Test
    fun `joinAndConnect should returnLobbyFlow`() {
        val lobbyFlow = mockk<Flow<List<String>>>()
        coEvery { lobbiesRepository.joinLobby(any(), any()) } returns "id"
        coEvery { lobbiesRepository.listenToLobby(GameCredentials("l", "id")) } returns lobbyFlow
        val result = runBlocking { connectLobbyUseCase.joinAndConnect("l") }
        assertThat(result.first).isEqualTo(lobbyFlow)
    }

    @Test
    fun `joinAndConnect should returnGamestateFlow`() {
        val gameStateFlow = mockk<Flow<GameState?>>()
        coEvery { lobbiesRepository.joinLobby(any(), any()) } returns "id"
        coEvery { gameRepository.getUpdates(GameCredentials("l", "id")) } returns gameStateFlow
        val result = runBlocking { connectLobbyUseCase.joinAndConnect("l") }
        assertThat(result.second).isEqualTo(gameStateFlow)
    }
}