package com.opendurak.opendurakandroid.domain.interactor

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakandroid.domain.repository.UsernameRepository
import io.mockk.mockk
import org.junit.Test

class UsernameUseCaseTest {
    private val usernameRepository = mockk<UsernameRepository>(relaxUnitFun = true)
    private val usernameUseCase = UsernameUseCase(usernameRepository)

    @Test
    fun `when nameIsTooShort isUsernameValid should returnFalse`() {
        assertThat(usernameUseCase.isUsernameValid("Pi")).isFalse()
    }

    @Test
    fun `isUsernameValid should returnTrue`() {
        assertThat(usernameUseCase.isUsernameValid("Lambda")).isTrue()
    }
}