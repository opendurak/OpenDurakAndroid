plugins {
    `java-library`
    kotlin("jvm")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

dependencies {
    api(platform("org.jetbrains.kotlin:kotlin-bom"))
    api(platform("org.jetbrains.kotlinx:kotlinx-coroutines-bom:1.6.4"))

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    api("com.opendurak:OpenDurakApi:1.0.0-SNAPSHOT")

    testImplementation("junit:junit:4.13.2")
    testImplementation("com.google.truth:truth:1.1.5")
    testImplementation("io.mockk:mockk:1.13.4")
}
