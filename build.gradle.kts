plugins {
    id("org.jetbrains.kotlinx.kover") version "0.6.1"
    kotlin("jvm") version "1.7.10" apply false
    kotlin("android") version "1.7.10" apply false
    id("com.android.application") version "7.4.2" apply false
    id("com.android.library") version "7.4.2" apply false
    id("com.gaelmarhic.quadrant") version "1.7" apply false
}
extra.apply {
    set("compose_compiler_version", "1.3.0")
    set("minSdk", 26)
    set("compileSdk", 33)
    set("targetSdk", 32)
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

subprojects {
    apply(plugin = "org.jetbrains.kotlinx.kover")
}

koverMerged.enable()