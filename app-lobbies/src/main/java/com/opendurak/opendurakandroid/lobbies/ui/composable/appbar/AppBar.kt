package com.opendurak.opendurakandroid.lobbies.ui.composable.appbar

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun AppBar(
    isSearchOpen: Boolean,
    searchText: String,
    changeSearchText: (String) -> Unit,
    openSearch: () -> Unit,
    closeSearch: () -> Unit,
    search: (String) -> Unit,
    openSettings: () -> Unit,
    modifier: Modifier = Modifier,
) {
    if (isSearchOpen) {
        SearchAppBar(
            modifier = modifier,
            text = searchText,
            changeText = changeSearchText,
            goBack = closeSearch,
            search = search,
        )
    } else {
        NormalAppBar(
            modifier = modifier,
            openSearch = openSearch,
            openSettings = openSettings
        )
    }
}
