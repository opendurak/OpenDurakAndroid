package com.opendurak.opendurakandroid.lobbies

import android.content.Intent
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.gaelmarhic.quadrant.QuadrantConstants
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.opendurak.opendurakandroid.common.Consts.EXTRA_LOBBY_ID
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobbies.ui.composable.LobbiesList
import com.opendurak.opendurakandroid.lobbies.ui.composable.SettingsDialog
import com.opendurak.opendurakandroid.lobbies.ui.composable.appbar.AppBar

class LobbiesActivity : ComponentActivity() {
    private val lobbiesViewModel by viewModels<LobbiesViewModel> {
        LobbiesViewModelFactory()
    }
    private val settingsViewModel by viewModels<SettingsViewModel> {
        SettingsViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)

        lobbiesViewModel.lobbyToOpen.observe(this) {
            val lobbyActivityIntent = Intent()
            lobbyActivityIntent.setClassName(this, QuadrantConstants.LOBBY_ACTIVITY)
            when (it) {
                is LobbiesViewModel.LobbyToJoin.NewLobby -> {
                    lobbyActivityIntent.putExtra(EXTRA_LOBBY_ID, it.lobbyId)
                    startActivity(lobbyActivityIntent)
                }
                is LobbiesViewModel.LobbyToJoin.SavedLobby -> {
                    startActivity(lobbyActivityIntent)
                }
            }
        }

        setContent {
            val lobbyIds by lobbiesViewModel.lobbyIds.observeAsState(emptyList())
            val isRefreshing by lobbiesViewModel.isRefreshing.observeAsState(false)
            val refresh = { lobbiesViewModel.refresh() }
            val onLobbySelected = { lobbyId: String -> lobbiesViewModel.selectLobby(lobbyId) }
            val isSearchOpen by lobbiesViewModel.isSearchOpen.observeAsState(initial = false)
            val searchText by lobbiesViewModel.searchText.observeAsState(initial = "")
            val settings by settingsViewModel.showSettings.observeAsState(initial = null)

            Ui(
                lobbyIds, isRefreshing, refresh,
                { lobbiesViewModel.addLobby() },
                onLobbySelected,
                isSearchOpen,
                searchText,
                { lobbiesViewModel.changeSearchText(it) },
                { lobbiesViewModel.openSearch() },
                { lobbiesViewModel.closeSearch() },
                { lobbiesViewModel.searchForLobby(it) },
                settings,
                { settingsViewModel.openSettings() },
                { settingsViewModel.confirmSettings(it) },
                { settingsViewModel.dismissSettings(it) },
                { settingsViewModel.updateShownSettings(it) },
            )
        }
    }
}

@Composable
private fun Ui(
    lobbyIds: List<String>,
    isRefreshing: Boolean,
    refresh: () -> Unit,
    addLobby: () -> Unit,
    selectLobby: (String) -> Unit,
    isSearchOpen: Boolean,

    searchText: String,
    changeSearchText: (String) -> Unit,
    openSearch: () -> Unit,
    closeSearch: () -> Unit,
    search: (String) -> Unit,
    settingsToShow: SettingsViewModel.Settings?,

    openSettings: () -> Unit,
    confirmSettings: (SettingsViewModel.Settings) -> Unit,
    dismissSettings: (SettingsViewModel.Settings) -> Unit,
    updateShownSettings: (SettingsViewModel.Settings) -> Unit,
    modifier: Modifier = Modifier,
) {
    OpenDurakAndroidTheme {

        Surface(
            modifier = modifier,
            color = MaterialTheme.colors.background,
            contentColor = MaterialTheme.colors.primaryVariant,
        ) {

            if (settingsToShow != null) {
                SettingsDialog(
                    settingsToShow = settingsToShow,
                    confirmSettings = confirmSettings,
                    dismissSettings = dismissSettings,
                    updateShownSettings = updateShownSettings
                )
            }

            Scaffold(
                topBar = {
                    AppBar(
                        isSearchOpen, searchText, changeSearchText,
                        openSearch, closeSearch, search, openSettings,
                    )
                },
                floatingActionButton = {
                    FloatingActionButton(onClick = { addLobby() }) {
                        Icon(
                            imageVector = Icons.Default.Add,
                            contentDescription = stringResource(R.string.add_lobby)
                        )
                    }
                },
            ) {
                LobbiesList(
                    lobbyIds = lobbyIds,
                    rememberSwipeRefreshState(isRefreshing),
                    refresh,
                    selectLobby,
                    Modifier.padding(it),
                )
            }
        }
    }
}

@Preview(showBackground = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun DarkPreview() {
    Ui(
        lobbyIds = listOf("asd", "fgh", "hjk"),
        isRefreshing = false,
        {}, {}, {},
        false, "", {}, {}, {}, {},
        null, {}, {}, {}, {},
    )
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    Ui(
        lobbyIds = listOf("asd", "fgh", "hjk"),
        isRefreshing = false,
        {}, {}, {},
        false, "", {}, {}, {}, {},
        null, {}, {}, {}, {},
    )
}