package com.opendurak.opendurakandroid.lobbies.ui.composable.appbar

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobbies.R

@Composable
fun NormalAppBar(
    openSearch: () -> Unit,
    openSettings: () -> Unit,
    modifier: Modifier = Modifier,
) {
    TopAppBar(
        modifier = modifier,
        title = { Text(text = stringResource(R.string.join_a_lobby)) },
        actions = {
            IconButton(onClick = openSettings) {
                Icon(
                    imageVector = Icons.Default.Settings,
                    contentDescription = stringResource(R.string.settings)
                )
            }
            IconButton(onClick = openSearch) {
                Icon(
                    imageVector = Icons.Default.Search,
                    contentDescription = stringResource(R.string.search)
                )
            }
        },
        elevation = 12.dp
    )
}

@Preview
@Composable
private fun Preview() {
    OpenDurakAndroidTheme {
        Surface(
            color = MaterialTheme.colors.background) {
            NormalAppBar({}, {})
        }
    }
}

@Preview
@Composable
private fun DarkPreview() {
    OpenDurakAndroidTheme(darkTheme = true) {
        Surface(
            color = MaterialTheme.colors.background) {
            NormalAppBar({}, {})
        }
    }
}