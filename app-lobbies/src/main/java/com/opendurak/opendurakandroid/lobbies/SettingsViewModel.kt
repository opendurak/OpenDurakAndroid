package com.opendurak.opendurakandroid.lobbies

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.opendurak.opendurakandroid.domain.interactor.UsernameUseCase
import kotlinx.coroutines.launch

class SettingsViewModel(
    private val usernameUseCase: UsernameUseCase,
) : ViewModel() {
    private val _showSettings = MutableLiveData<Settings?>()
    val showSettings: LiveData<Settings?> = _showSettings

    init {
        viewModelScope.launch {
            val username = usernameUseCase.getUsername()

            if (!usernameUseCase.isUsernameValid(username))
                _showSettings.postValue(Settings(username, true, true))
            else _showSettings.postValue(null)
        }
    }

    fun openSettings() {
        viewModelScope.launch {
            val username = usernameUseCase.getUsername()
            _showSettings.postValue(
                Settings(username, !usernameUseCase.isUsernameValid(username), false)
            )
        }
    }

    fun dismissSettings(settings: Settings) {
        if (!settings.validSettingsNeeded) _showSettings.postValue(null)
    }

    fun updateShownSettings(settings: Settings) {
        _showSettings.postValue(
            settings.copy(hasUserNameError = !usernameUseCase.isUsernameValid(settings.username))
        )
    }

    fun confirmSettings(settings: Settings) {
        if (!settings.hasUserNameError) {
            viewModelScope.launch {
                usernameUseCase.setUsername(settings.username)
                _showSettings.postValue(null)
            }
        }
    }

    data class Settings(
        val username: String,
        val hasUserNameError: Boolean,
        val validSettingsNeeded: Boolean,
    )
}