package com.opendurak.opendurakandroid.lobbies.ui.composable

import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.opendurak.opendurakandroid.lobbies.R
import com.opendurak.opendurakandroid.lobbies.SettingsViewModel

@Composable
fun SettingsDialog(
    settingsToShow: SettingsViewModel.Settings,
    confirmSettings: (SettingsViewModel.Settings) -> Unit,
    dismissSettings: (SettingsViewModel.Settings) -> Unit,
    updateShownSettings: (SettingsViewModel.Settings) -> Unit,
    modifier: Modifier = Modifier,
) {
    AlertDialog(
        modifier = modifier,
        title = { Text(text = stringResource(R.string.settings)) },
        text = {
            TextField(
                label = { Text(text = stringResource(R.string.username)) },
                value = settingsToShow.username,
                isError = settingsToShow.hasUserNameError,
                onValueChange = { updateShownSettings(settingsToShow.copy(username = it)) })
        },
        onDismissRequest = {
            dismissSettings(settingsToShow)
        },
        buttons = {
            TextButton(
                enabled = !settingsToShow.hasUserNameError,
                onClick = { confirmSettings(settingsToShow) }
            ) {
                Text(text = stringResource(R.string.ok))
            }
        }
    )
}