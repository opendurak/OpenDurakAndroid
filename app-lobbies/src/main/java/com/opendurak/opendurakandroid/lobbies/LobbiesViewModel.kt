package com.opendurak.opendurakandroid.lobbies

import androidx.lifecycle.*
import com.opendurak.opendurakandroid.common.CombinedLiveData
import com.opendurak.opendurakandroid.common.SingleLiveEvent
import com.opendurak.opendurakandroid.domain.interactor.ConnectLobbyUseCase
import com.opendurak.opendurakandroid.domain.interactor.CreateLobbyUseCase
import com.opendurak.opendurakandroid.domain.interactor.GetLobbiesUseCase
import kotlinx.coroutines.launch

class LobbiesViewModel(
    private val createLobbyUseCase: CreateLobbyUseCase,
    private val connectLobbyUseCase: ConnectLobbyUseCase,
    private val getLobbiesUseCase: GetLobbiesUseCase,
) : ViewModel() {

    private val allLobbies = MutableLiveData<List<String>>()
    private val currentSearchText = MutableLiveData("")

    val lobbyIds: LiveData<List<String>>

    private val _isRefreshing = MutableLiveData(false)
    val isRefreshing: LiveData<Boolean> = _isRefreshing

    private val _searchText = MutableLiveData("")
    val searchText: LiveData<String> = _searchText

    private val _isSearchOpen = MutableLiveData(false)
    val isSearchOpen: LiveData<Boolean> = _isSearchOpen

    private val _lobbyToOpen = SingleLiveEvent<LobbyToJoin>()
    val lobbyToOpen: LiveData<LobbyToJoin> = _lobbyToOpen

    init {
        viewModelScope.launch {
            val hasSavedLobby = connectLobbyUseCase.getJoinedLobbyId() != null
            if (hasSavedLobby) _lobbyToOpen.postValue(LobbyToJoin.SavedLobby)
        }

        lobbyIds = Transformations.distinctUntilChanged(
            Transformations.map(CombinedLiveData(allLobbies, currentSearchText)) {
                val query = it.second
                if (query != null) it.first?.filter { allLobbies -> allLobbies.contains(query) }
                    ?: emptyList()
                else emptyList()
            }
        )

        viewModelScope.launch {
            val lobbyIds = getLobbiesUseCase.execute()
            allLobbies.postValue(lobbyIds)
        }
    }

    fun refresh() {
        viewModelScope.launch {
            _isRefreshing.postValue(true)
            val lobbyIds = getLobbiesUseCase.execute()
            allLobbies.postValue(lobbyIds)
            _isRefreshing.postValue(false)
        }
    }

    fun selectLobby(lobbyId: String) = _lobbyToOpen.postValue(LobbyToJoin.NewLobby(lobbyId))

    fun changeSearchText(query: String) = _searchText.postValue(query)

    fun searchForLobby(query: String) = currentSearchText.postValue(query)

    fun openSearch() = _isSearchOpen.postValue(true)
    fun closeSearch() = _isSearchOpen.postValue(false)

    fun addLobby() {
        viewModelScope.launch {
            createLobbyUseCase.execute()
            val lobbyIds = getLobbiesUseCase.execute()
            allLobbies.postValue(lobbyIds)
        }
    }

    sealed interface LobbyToJoin {
        object SavedLobby : LobbyToJoin

        data class NewLobby(val lobbyId: String) : LobbyToJoin
    }
}