package com.opendurak.opendurakandroid.lobbies.ui.composable

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.SwipeRefreshState

@Composable
fun LobbiesList(
    lobbyIds: List<String>,
    swipeRefreshState: SwipeRefreshState,
    refresh: () -> Unit,
    onSelected: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    SwipeRefresh(
        modifier = modifier
            .fillMaxWidth()
            .fillMaxHeight(),
        state = swipeRefreshState,
        onRefresh = { refresh() }) {
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
        ) {
            items(lobbyIds.size, itemContent = {
                LobbyListItem(lobbyIds[it], onSelected)
            })
        }
    }
}