package com.opendurak.opendurakandroid.lobbies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.opendurak.opendurakandroid.common.Injector

class LobbiesViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return LobbiesViewModel(
            Injector.createLobbyUseCase,
            Injector.connectLobbyUseCase,
            Injector.getLobbiesUseCase,
        ) as T
    }
}