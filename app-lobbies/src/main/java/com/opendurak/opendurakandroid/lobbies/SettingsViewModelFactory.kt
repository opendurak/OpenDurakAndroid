package com.opendurak.opendurakandroid.lobbies

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.opendurak.opendurakandroid.common.Injector

class SettingsViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return SettingsViewModel(
            Injector.usernameUseCase,
        ) as T
    }
}