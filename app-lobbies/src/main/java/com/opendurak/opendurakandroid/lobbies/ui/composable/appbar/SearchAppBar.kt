package com.opendurak.opendurakandroid.lobbies.ui.composable.appbar

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Clear
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobbies.R

@Composable
fun SearchAppBar(
    text: String,
    changeText: (String) -> Unit,
    goBack: () -> Unit,
    search: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    TopAppBar(
        modifier = modifier
            .fillMaxWidth()
            .height(56.dp),
        navigationIcon = {
            IconButton(
                onClick = { changeText(""); search(""); goBack() }
            ) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = stringResource(R.string.back),
                )
            }
        },
        actions = {
            if (text.isNotEmpty()) {
                IconButton(
                    onClick = { changeText(""); search("") }
                ) {
                    Icon(
                        imageVector = Icons.Default.Clear,
                        contentDescription = stringResource(R.string.clear),
                    )
                }
            }
        },
        title = {
            TextField(
                value = text,
                onValueChange = { changeText(it) },
                placeholder = {
                    Text(
                        fontSize = MaterialTheme.typography.subtitle1.fontSize,
                        color = Color.White,
                        modifier = Modifier.alpha(ContentAlpha.medium),
                        text = stringResource(R.string.search_here),
                    )
                },
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    cursorColor = Color.White,
                ),
                textStyle = TextStyle(
                    fontSize = MaterialTheme.typography.subtitle1.fontSize,
                    color = Color.White
                ),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Search
                ),
                keyboardActions = KeyboardActions {
                    search(text)
                }
            )
        }
    )
}

@Preview
@Composable
private fun SearchAppBarPreview() {
    OpenDurakAndroidTheme {
        Surface(
            color = MaterialTheme.colors.background) {
            SearchAppBar(text = "Something", {}, {}, {})
        }
    }
}

@Preview
@Composable
private fun SearchAppBarBlankPreview() {
    OpenDurakAndroidTheme {
        Surface(
            color = MaterialTheme.colors.background) {
            SearchAppBar(text = "", {}, {}, {})
        }
    }
}

@Preview
@Composable
private fun SearchAppBarDarkPreview() {
    OpenDurakAndroidTheme(darkTheme = true) {
        Surface(
            color = MaterialTheme.colors.background) {
            SearchAppBar(text = "Something", {}, {}, {})
        }
    }
}

@Preview
@Composable
private fun SearchAppBarBlankDarkPreview() {
    OpenDurakAndroidTheme(darkTheme = true) {
        Surface(
            color = MaterialTheme.colors.background) {
            SearchAppBar(text = "", {}, {}, {})
        }
    }
}