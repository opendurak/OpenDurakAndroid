package com.opendurak.opendurakandroid.lobbies

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.jraska.livedata.TestObserver
import com.jraska.livedata.test
import com.opendurak.opendurakandroid.domain.interactor.UsernameUseCase
import io.mockk.*
import kotlinx.coroutines.test.StandardTestDispatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SettingsViewModelTest {

    private val usernameUseCase = mockk<UsernameUseCase>()

    private lateinit var settingsViewModel: SettingsViewModel

    private val testDispatcher = StandardTestDispatcher()

    @get:Rule
    val dispatcher = TestDispatcherRule(testDispatcher)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var settings: TestObserver<SettingsViewModel.Settings?>

    @Before
    fun setUp() {
        coEvery { usernameUseCase.setUsername(any()) } just Runs
        coEvery { usernameUseCase.isUsernameValid("") } returns false
        coEvery { usernameUseCase.isUsernameValid("aaa") } returns true
        settingsViewModel = SettingsViewModel(usernameUseCase)
        settings = settingsViewModel.showSettings.test()
    }

    private fun flushViewModelScope() = testDispatcher.scheduler.runCurrent()

    private fun changeUsername(username: String) {
        settingsViewModel.updateShownSettings(
            settings.value()!!.copy(username = username)
        )
    }

    @Test
    fun `when savedUsernameIsValid init should notOpenSettings`() {
        coEvery { usernameUseCase.getUsername() } returns "aaa"
        flushViewModelScope()

        assertThat(settings.value()).isEqualTo(null)
    }

    @Test
    fun `when savedUsernameIsNotValid init should openSettings`() {
        coEvery { usernameUseCase.getUsername() } returns ""
        flushViewModelScope()

        assertThat(settings.value()).isEqualTo(
            SettingsViewModel.Settings("", true, true)
        )
    }

    @Test
    fun `openSettings should openSettings`() {
        coEvery { usernameUseCase.getUsername() } returns "aaa"
        settingsViewModel.openSettings()
        flushViewModelScope()

        assertThat(settings.value()).isEqualTo(
            SettingsViewModel.Settings("aaa", false, false)
        )
    }

    @Test
    fun `when usernameValid updateShownSettings should setUsernameErrorTrue`() {
        coEvery { usernameUseCase.getUsername() } returns "aaa"
        settingsViewModel.openSettings()
        flushViewModelScope()
        changeUsername("")

        assertThat(settings.value()).isEqualTo(
            SettingsViewModel.Settings("", true, false)
        )
    }

    @Test
    fun `when usernameInvalid updateShownSettings should setUsernameErrorFalse`() {
        coEvery { usernameUseCase.getUsername() } returns ""
        flushViewModelScope()
        changeUsername("aaa")

        assertThat(settings.value()).isEqualTo(
            SettingsViewModel.Settings("aaa", false, true)
        )
    }

    @Test
    fun `when noValidUsernameSaved dismissSettings should notCloseSettings`() {
        coEvery { usernameUseCase.getUsername() } returns ""
        flushViewModelScope()
        settingsViewModel.dismissSettings(settings.value()!!)

        assertThat(settings.value()).isEqualTo(
            SettingsViewModel.Settings("", true, true)
        )
    }

    @Test
    fun `when validUsernameSaved dismissSettings should closeSettings`() {
        coEvery { usernameUseCase.getUsername() } returns "aaa"
        settingsViewModel.openSettings()
        flushViewModelScope()
        settingsViewModel.dismissSettings(settings.value()!!)

        assertThat(settings.value()).isNull()
    }

    @Test
    fun `when validUsernameSavedAndCurrentUsernameInvalid dismissSettings should closeSettings`() {
        coEvery { usernameUseCase.getUsername() } returns "aaa"
        settingsViewModel.openSettings()
        flushViewModelScope()
        changeUsername("")

        settingsViewModel.dismissSettings(settings.value()!!)

        assertThat(settings.value()).isNull()
    }

    @Test
    fun `confirmSettings should closeSettings`() {
        coEvery { usernameUseCase.getUsername() } returns "aaa"
        settingsViewModel.openSettings()
        flushViewModelScope()
        settingsViewModel.confirmSettings(settings.value()!!)
        flushViewModelScope()

        assertThat(settings.value()).isNull()
    }

    @Test
    fun `confirmSettings should saveUsername`() {
        coEvery { usernameUseCase.getUsername() } returns "aaa"
        settingsViewModel.openSettings()
        flushViewModelScope()
        settingsViewModel.confirmSettings(settings.value()!!)
        flushViewModelScope()

        coVerify(exactly = 1) { usernameUseCase.setUsername("aaa") }
    }

    @Test
    fun `when usernameHasError confirmSettings should notSaveUsername`() {
        coEvery { usernameUseCase.getUsername() } returns "aaa"
        settingsViewModel.openSettings()
        flushViewModelScope()
        changeUsername("")

        settingsViewModel.confirmSettings(settings.value()!!)
        flushViewModelScope()

        coVerify(exactly = 0) { usernameUseCase.setUsername(any()) }
    }
}