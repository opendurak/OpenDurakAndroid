package com.opendurak.opendurakandroid.lobbies

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.jraska.livedata.TestObserver
import com.jraska.livedata.test
import com.opendurak.opendurakandroid.domain.interactor.ConnectLobbyUseCase
import com.opendurak.opendurakandroid.domain.interactor.CreateLobbyUseCase
import com.opendurak.opendurakandroid.domain.interactor.GetLobbiesUseCase
import com.opendurak.opendurakandroid.lobbies.LobbiesViewModel.LobbyToJoin.SavedLobby
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.StandardTestDispatcher
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class LobbiesViewModelTest {
    private lateinit var lobbiesViewModel: LobbiesViewModel

    private val createLobbyUseCase = mockk<CreateLobbyUseCase>(relaxUnitFun = true)
    private val connectLobbyUseCase = mockk<ConnectLobbyUseCase>(relaxUnitFun = true) {
        coEvery { getJoinedLobbyId() } returns null
    }
    private val getLobbiesUseCase = mockk<GetLobbiesUseCase>(relaxUnitFun = true) {
        coEvery { execute() } returns emptyList()
    }

    private val testDispatcher = StandardTestDispatcher()

    @get:Rule
    val dispatcher = TestDispatcherRule(testDispatcher)

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var isRefreshing: TestObserver<Boolean>
    private lateinit var isSearchOpen: TestObserver<Boolean>
    private lateinit var lobbyIds: TestObserver<List<String>>
    private lateinit var lobbyToOpen: TestObserver<LobbiesViewModel.LobbyToJoin>
    private lateinit var searchText: TestObserver<String>

    @Before
    fun setUp() {
        lobbiesViewModel = LobbiesViewModel(
            createLobbyUseCase, connectLobbyUseCase, getLobbiesUseCase)
        isRefreshing = lobbiesViewModel.isRefreshing.test()
        isSearchOpen = lobbiesViewModel.isSearchOpen.test()
        lobbyIds = lobbiesViewModel.lobbyIds.test()
        lobbyToOpen = lobbiesViewModel.lobbyToOpen.test()
        searchText = lobbiesViewModel.searchText.test()
    }

    private fun flushViewModelScope() = testDispatcher.scheduler.runCurrent()

    @Test
    fun `init shouldReturnEmptyLobbyIds`() {
        flushViewModelScope()

        assertThat(lobbyIds.value()).isEmpty()
    }

    @Test
    fun `when noSavedLobby init shouldNotOpenLobby`() {
        flushViewModelScope()

        lobbyToOpen.assertNoValue()
    }

    @Test
    fun `addLobby shouldCreateLobby`() {
        flushViewModelScope()

        lobbiesViewModel.addLobby()
        flushViewModelScope()

        coVerify(exactly = 1) { createLobbyUseCase.execute() }
    }

    @Test
    fun `addLobby shouldRefreshLobbies`() {
        flushViewModelScope()

        coEvery { getLobbiesUseCase.execute() } returns listOf("a")
        lobbiesViewModel.addLobby()
        flushViewModelScope()

        assertThat(lobbyIds.value()).containsExactly("a")
    }

    @Test
    fun `refresh shouldShowRefreshing`() {
        flushViewModelScope()

        lobbiesViewModel.refresh()
        flushViewModelScope()

        val history = isRefreshing.valueHistory()
        assertThat(history.last()).isFalse()
        assertThat(history[history.lastIndex - 1]).isTrue()
    }

    @Test
    fun `refresh shouldGetNewLobbies`() {
        flushViewModelScope()

        coEvery { getLobbiesUseCase.execute() } returns listOf("a")
        lobbiesViewModel.refresh()
        flushViewModelScope()

        assertThat(lobbyIds.value()).containsExactly("a")
    }

    @Test
    fun `when thereIsASavedLobby init shouldOpenThat`() {
        coEvery { connectLobbyUseCase.getJoinedLobbyId() } returns "a"
        flushViewModelScope()

        assertThat(lobbyToOpen.value()).isEqualTo(SavedLobby)
    }

    @Test
    fun `searchForLobby shouldFilterLobbies`() {
        coEvery { getLobbiesUseCase.execute() } returns listOf("a", "ab", "b")
        lobbiesViewModel.searchForLobby("a")
        flushViewModelScope()

        assertThat(lobbyIds.value()).containsExactly("a", "ab")
    }
}