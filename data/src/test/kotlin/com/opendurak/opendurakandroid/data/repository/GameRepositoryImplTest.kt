package com.opendurak.opendurakandroid.data.repository

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakandroid.data.MockLogTestRule
import com.opendurak.opendurakandroid.data.api.GameApi
import com.opendurak.opendurakandroid.data.api.GameWebsocketApi
import com.opendurak.opendurakandroid.domain.model.GameCredentials
import com.opendurak.opendurakapi.GameState
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.isActive
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withTimeoutOrNull
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

public class GameRepositoryImplTest {
    @get:Rule
    public val mockLogTestRule: MockLogTestRule = MockLogTestRule()

    private val gameApi = mockk<GameApi>(relaxUnitFun = true)
    private val gameWebsocketApi = mockk<GameWebsocketApi>(relaxUnitFun = true)
    private val getCurrentMs = mockk<() -> Long> {
        every { this@mockk.invoke() } returns System.currentTimeMillis()
    }
    private val gameRepositoryImpl = GameRepositoryImpl(gameApi, gameWebsocketApi, getCurrentMs)
    private val gameStateOfWorkingConnection = mockk<GameState>()
    private val gameStateOfCrashingConnection = mockk<GameState>()

    private companion object {
        const val TIME_BETWEEN_EMITS = 1000L
        const val TIME_SHORT_RETRY = 1000L
        const val TIME_LONG_RETRY = 5000L
    }

    @Test
    public fun `when flowCrashes getUpdates should retryWithNewValues`() {
        var answerNr = 0
        coEvery { gameWebsocketApi.getGameStateFlow(any(), any()) } returns
                flow { if (answerNr++ != 1) emitCrashing() else emitHealthy() }

        runTest {
            val flow =
                gameRepositoryImpl.getUpdates(GameCredentials("l", "u"))
            var hasRestarted = false
            withTimeoutOrNull(TIME_BETWEEN_EMITS + TIME_SHORT_RETRY + 1) {
                flow.collect {
                    if (it == gameStateOfWorkingConnection) hasRestarted = true
                }
            }
            if (!hasRestarted) Assert.fail()
        }
    }

    @Test
    public fun `when flowCrashes getUpdates should retryFast`() {
        coEvery { gameWebsocketApi.getGameStateFlow(any(), any()) } returns
                flow { emitCrashing() }

        runTest {
            val flow =
                gameRepositoryImpl.getUpdates(GameCredentials("l", "u"))
            var flowStarts = 0
            withTimeoutOrNull(TIME_BETWEEN_EMITS + TIME_SHORT_RETRY + 1) {
                flow.collect {
                    if (it == gameStateOfCrashingConnection) flowStarts++
                }
            }
            assertThat(flowStarts).isEqualTo(2)
        }
    }

    @Test
    public fun `when flowCrashesRepeatedly getUpdates should notStartFastTheThirdTime`() {
        coEvery { gameWebsocketApi.getGameStateFlow(any(), any()) } returns
                flow { emitCrashing() }

        runTest {
            val flow =
                gameRepositoryImpl.getUpdates(GameCredentials("l", "u"))
            var flowStarts = 0
            withTimeoutOrNull(2 * TIME_BETWEEN_EMITS + 2 * TIME_SHORT_RETRY + 1) {
                flow.collect {
                    if (it == gameStateOfCrashingConnection) flowStarts++
                }
            }
            assertThat(flowStarts).isEqualTo(2)
        }
    }

    @Test
    public fun `when flowCrashesRepeatedly getUpdates should retryThirdTime`() {
        coEvery { gameWebsocketApi.getGameStateFlow(any(), any()) } returns
                flow { emitCrashing() }

        runTest {
            val flow =
                gameRepositoryImpl.getUpdates(GameCredentials("l", "u"))
            var flowStarts = 0
            withTimeoutOrNull(2 * TIME_BETWEEN_EMITS + TIME_SHORT_RETRY + TIME_LONG_RETRY + 1) {
                flow.collect {
                    if (it == gameStateOfCrashingConnection) flowStarts++
                }
            }
            assertThat(flowStarts).isEqualTo(3)
        }
    }

    @Test
    public fun `when waitingForLongTime getUpdates should retryFastAgain`() {
        every { getCurrentMs.invoke() } returnsMany listOf(1_00000L, 1_30000L)

        coEvery { gameWebsocketApi.getGameStateFlow(any(), any()) } returns
                flow { emitCrashing() }

        runTest {
            val flow =
                gameRepositoryImpl.getUpdates(GameCredentials("l", "u"))
            var flowStarts = 0
            withTimeoutOrNull(2 * TIME_BETWEEN_EMITS + 2 * TIME_SHORT_RETRY + 1) {
                flow.collect {
                    if (it == gameStateOfCrashingConnection) flowStarts++
                }
            }
            assertThat(flowStarts).isEqualTo(3)
        }
    }

    private suspend fun FlowCollector<GameState?>.emitCrashing() {
        println("emitting")
        emit(gameStateOfCrashingConnection)
        println("waiting 1s")
        delay(TIME_BETWEEN_EMITS)
        throw Exception()
    }

    private suspend fun FlowCollector<GameState?>.emitHealthy() {
        while (currentCoroutineContext().isActive) {
            emit(gameStateOfWorkingConnection)
            delay(TIME_BETWEEN_EMITS)
        }
    }
}