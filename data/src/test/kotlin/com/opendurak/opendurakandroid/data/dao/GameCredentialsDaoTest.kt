package com.opendurak.opendurakandroid.data.dao


import android.content.SharedPreferences
import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakandroid.domain.model.GameCredentials
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Test

public class GameCredentialsDaoTest {
    private val sharedPreferences = mockk<SharedPreferences>(relaxed = true)
    private val gameCredentialsDao = GameCredentialsDao(
        mockk { every { getSharedPreferences(any(), any()) } returns sharedPreferences }
    )

    @Test
    public fun `when lobbyIdIsNotSet get should returnNull`() {
        every { sharedPreferences.getString("lobby", any()) } returns null

        assertThat(runBlocking { gameCredentialsDao.get() }).isNull()
    }

    @Test
    public fun `when userIdIsNotSet get should returnNull`() {
        every { sharedPreferences.getString("user", any()) } returns null

        assertThat(runBlocking { gameCredentialsDao.get() }).isNull()
    }

    @Test
    public fun `get should returnGameCredentials`() {
        every { sharedPreferences.getString("lobby", any()) } returns "l"
        every { sharedPreferences.getString("user", any()) } returns "u"

        assertThat(runBlocking { gameCredentialsDao.get() })
            .isEqualTo(GameCredentials("l", "u"))
    }
}