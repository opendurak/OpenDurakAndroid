package com.opendurak.opendurakandroid.data

import android.util.Log
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

public class MockLogTestRule : TestRule {
    override fun apply(base: Statement, description: Description?): Statement =
        object : Statement() {
            override fun evaluate() {
                mockkStatic(Log::class) {
                    every { Log.v(any(), any()) } answers {
                        println("[V:${this.firstArg<String>()}] ${this.secondArg<String>()}")
                        0
                    }
                    every { Log.d(any(), any()) } answers {
                        println("[D:${this.firstArg<String>()}] ${this.secondArg<String>()}")
                        0
                    }
                    every { Log.i(any(), any()) } answers {
                        println("[I:${this.firstArg<String>()}] ${this.secondArg<String>()}")
                        0
                    }
                    every { Log.w(any(), any<String>()) } answers {
                        println("[W:${this.firstArg<String>()}] ${this.secondArg<String>()}")
                        0
                    }
                    every { Log.w(any(), any<Throwable>()) } answers {
                        println("[W:${this.firstArg<String>()}] ${this.secondArg<Throwable>()}")
                        0
                    }
                    every { Log.e(any(), any()) } answers {
                        println("[E:${this.firstArg<String>()}] ${this.secondArg<String>()}")
                        0
                    }

                    base.evaluate()
                }
            }
        }

}