package com.opendurak.opendurakandroid.data.dao

import android.content.Context
import androidx.core.content.edit
import com.opendurak.opendurakandroid.domain.model.GameCredentials
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class GameCredentialsDao(context: Context) {
    private val sharedPreferences = context.getSharedPreferences(
        "game-credentials", Context.MODE_PRIVATE
    )
    private val keyLobby = "lobby"
    private val keyUser = "user"

    fun save(gameCredentials: GameCredentials) {
        sharedPreferences.edit {
            putString(keyLobby, gameCredentials.lobbyId)
            putString(keyUser, gameCredentials.userId)
        }
    }

    suspend fun get(): GameCredentials? = withContext(Dispatchers.IO) {
        val lobbyId = sharedPreferences.getString(keyLobby, null)
        val userId = sharedPreferences.getString(keyUser, null)
        if (lobbyId != null && userId != null) return@withContext GameCredentials(lobbyId, userId)
        else return@withContext null
    }

    fun clear() {
        sharedPreferences.edit { clear() }
    }
}