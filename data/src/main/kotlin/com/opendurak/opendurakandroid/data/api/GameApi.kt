package com.opendurak.opendurakandroid.data.api

import com.opendurak.opendurakapi.Card
import com.opendurak.opendurakapi.DefendRequest
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

internal interface GameApi {
    @POST("/lobby/{lobbyId}/game/start")
    suspend fun startGame(@Path("lobbyId") lobbyId: String, @Query("userId") userId: String)

    @POST("/lobby/{lobbyId}/game/attack")
    suspend fun attack(
        @Body card: Card,
        @Path("lobbyId") lobbyId: String,
        @Query("userId") userId: String,
    )

    @POST("/lobby/{lobbyId}/game/help")
    suspend fun help(
        @Body card: Card,
        @Path("lobbyId") lobbyId: String,
        @Query("userId") userId: String,
    )

    @POST("/lobby/{lobbyId}/game/defend")
    suspend fun defend(
        @Body defendRequest: DefendRequest,
        @Path("lobbyId") lobbyId: String,
        @Query("userId") userId: String,
    )

    @POST("/lobby/{lobbyId}/game/giveUp")
    suspend fun giveUpRound(@Path("lobbyId") lobbyId: String, @Query("userId") userId: String)
}