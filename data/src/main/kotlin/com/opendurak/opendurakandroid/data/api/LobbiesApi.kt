package com.opendurak.opendurakandroid.data.api

import retrofit2.http.GET
import retrofit2.http.PUT

internal interface LobbiesApi {

    @GET("/lobby")
    suspend fun getLobbies(): List<String>

    @PUT("/lobby")
    suspend fun createLobby(): String
}