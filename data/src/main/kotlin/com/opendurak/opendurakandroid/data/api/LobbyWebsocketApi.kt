package com.opendurak.opendurakandroid.data.api

import com.fasterxml.jackson.databind.ObjectMapper
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.callbackFlow
import okhttp3.*
import okio.ByteString
import java.net.URI

internal class LobbyWebsocketApi(
    private val baseUrl: String,
    private val objectMapper: ObjectMapper,
    private val okHttpClient: OkHttpClient,
) {
    suspend fun getLobbyFlow(userId: String, lobbyId: String) =
        callbackFlow {
            val webSocket = okHttpClient.newWebSocket(
                Request.Builder()
                    .url(
                        URI.create(baseUrl).resolve("/lobby/$lobbyId/listenLobby?userId=$userId")
                            .normalize()
                            .toURL()
                    )
                    .build(),
                object : WebSocketListener() {

                    override fun onOpen(webSocket: WebSocket, response: Response) {}

                    override fun onMessage(webSocket: WebSocket, text: String) {
                        val userNames =
                            objectMapper.readValue(text, Array<String>::class.java).toList()
                        trySendBlocking(userNames)
                    }

                    override fun onMessage(webSocket: WebSocket, bytes: ByteString) {}

                    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {}

                    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                        this@callbackFlow.close()
                    }

                    override fun onFailure(
                        webSocket: WebSocket,
                        t: Throwable,
                        response: Response?,
                    ) {
                        this@callbackFlow.close()
                    }
                }
            )

            awaitClose {
                webSocket.close(1000, "")
            }
        }
}