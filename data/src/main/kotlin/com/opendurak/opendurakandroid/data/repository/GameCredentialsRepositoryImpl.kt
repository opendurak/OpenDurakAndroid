package com.opendurak.opendurakandroid.data.repository

import com.opendurak.opendurakandroid.data.dao.GameCredentialsDao
import com.opendurak.opendurakandroid.domain.model.GameCredentials
import com.opendurak.opendurakandroid.domain.repository.GameCredentialsRepository

internal class GameCredentialsRepositoryImpl(
    private val gameCredentialsDao: GameCredentialsDao,
) : GameCredentialsRepository {

    override suspend fun saveGameCredentials(gameCredentials: GameCredentials) =
        gameCredentialsDao.save(gameCredentials)

    override suspend fun getSavedGameCredentials() = gameCredentialsDao.get()

    override suspend fun clearSavedGameCredentials() = gameCredentialsDao.clear()
}