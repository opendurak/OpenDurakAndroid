package com.opendurak.opendurakandroid.data.repository

import com.opendurak.opendurakandroid.data.dao.UsernameDao
import com.opendurak.opendurakandroid.domain.repository.UsernameRepository

internal class UsernameRepositoryImpl(
    private val usernameDao: UsernameDao,
) : UsernameRepository {
    override suspend fun getUsername(): String = usernameDao.get()

    override suspend fun setUsername(username: String) = usernameDao.save(username)
}