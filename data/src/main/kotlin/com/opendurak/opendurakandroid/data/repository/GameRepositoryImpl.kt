package com.opendurak.opendurakandroid.data.repository

import android.util.Log
import com.opendurak.opendurakandroid.data.api.GameApi
import com.opendurak.opendurakandroid.data.api.GameWebsocketApi
import com.opendurak.opendurakandroid.domain.model.GameCredentials
import com.opendurak.opendurakandroid.domain.repository.GameRepository
import com.opendurak.opendurakapi.Card
import com.opendurak.opendurakapi.DefendRequest
import com.opendurak.opendurakapi.GameState
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.retry

internal class GameRepositoryImpl(
    private val gameApi: GameApi,
    private val gameWebsocketApi: GameWebsocketApi,
    private val getCurrentMs: () -> Long,
) : GameRepository {
    override suspend fun getUpdates(gameCredentials: GameCredentials): Flow<GameState?> {
        var lastRetryAtMs = 0L
        return gameWebsocketApi.getGameStateFlow(
            gameCredentials.userId,
            gameCredentials.lobbyId
        ).retry {
            Log.d(GameWebsocketApi::class.simpleName,
                "Stopped collection because ${it::class.simpleName}")
            val now = getCurrentMs()
            val recentlyRestarted = lastRetryAtMs + 30_000L > now
            if (recentlyRestarted) delay(5000) else delay(1000)
            lastRetryAtMs = now
            true
        }
    }

    override suspend fun start(gameCredentials: GameCredentials) {
        return gameApi.startGame(gameCredentials.lobbyId, gameCredentials.userId)
    }

    override suspend fun attack(card: Card, gameCredentials: GameCredentials) {
        return gameApi.attack(card, gameCredentials.lobbyId, gameCredentials.userId)
    }

    override suspend fun defend(defendRequest: DefendRequest, gameCredentials: GameCredentials) {
        return gameApi.defend(defendRequest, gameCredentials.lobbyId, gameCredentials.userId)
    }

    override suspend fun help(card: Card, gameCredentials: GameCredentials) {
        return gameApi.help(card, gameCredentials.lobbyId, gameCredentials.userId)
    }

    override suspend fun giveUp(gameCredentials: GameCredentials) {
        return gameApi.giveUpRound(gameCredentials.lobbyId, gameCredentials.userId)
    }

}