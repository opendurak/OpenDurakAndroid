package com.opendurak.opendurakandroid.data.dao

import android.content.Context
import androidx.core.content.edit
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class UsernameDao(context: Context) {
    private val sharedPreferences = context.getSharedPreferences(
        "username", Context.MODE_PRIVATE
    )
    private val key = "username"

    fun save(username: String) {
        sharedPreferences.edit { putString(key, username) }
    }

    suspend fun get(): String = withContext(Dispatchers.IO) {
        return@withContext sharedPreferences.getString(key, "") ?: ""
    }
}