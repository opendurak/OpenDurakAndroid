package com.opendurak.opendurakandroid.data.api

import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

internal interface LobbyApi {
    @POST("/lobby/{lobbyId}/join")
    suspend fun join(@Path("lobbyId") lobbyId: String, @Query("username") userName: String): String
}