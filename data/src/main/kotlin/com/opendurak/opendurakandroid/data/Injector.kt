package com.opendurak.opendurakandroid.data

import android.content.Context
import com.fasterxml.jackson.databind.ObjectMapper
import com.opendurak.opendurakandroid.data.api.*
import com.opendurak.opendurakandroid.data.dao.GameCredentialsDao
import com.opendurak.opendurakandroid.data.dao.UsernameDao
import com.opendurak.opendurakandroid.data.repository.GameCredentialsRepositoryImpl
import com.opendurak.opendurakandroid.data.repository.GameRepositoryImpl
import com.opendurak.opendurakandroid.data.repository.LobbiesRepositoryImpl
import com.opendurak.opendurakandroid.data.repository.UsernameRepositoryImpl
import com.opendurak.opendurakandroid.domain.repository.GameCredentialsRepository
import com.opendurak.opendurakandroid.domain.repository.GameRepository
import com.opendurak.opendurakandroid.domain.repository.LobbiesRepository
import com.opendurak.opendurakandroid.domain.repository.UsernameRepository
import okhttp3.OkHttpClient
import retrofit2.Retrofit

public class Injector(
    private val baseUrl: String,
    private val retrofit: Retrofit,
    private val okHttpClient: OkHttpClient,
    private val objectMapper: ObjectMapper,
    private val context: Context,
) {
    private val gameCredentialsDao by lazy { GameCredentialsDao(context) }
    private val usernameDao by lazy { UsernameDao(context) }

    private val gameApi by lazy { retrofit.create(GameApi::class.java) }
    private val lobbiesApi by lazy { retrofit.create(LobbiesApi::class.java) }
    private val lobbyApi by lazy { retrofit.create(LobbyApi::class.java) }

    private val gameWebsocketApi by lazy { GameWebsocketApi(baseUrl, objectMapper, okHttpClient) }
    private val lobbyWebsocketApi by lazy { LobbyWebsocketApi(baseUrl, objectMapper, okHttpClient) }

    public val gameRepository: GameRepository by lazy {
        GameRepositoryImpl(gameApi, gameWebsocketApi, { System.currentTimeMillis() })
    }
    public val lobbiesRepository: LobbiesRepository by lazy {
        LobbiesRepositoryImpl(lobbiesApi,
            lobbyApi,
            lobbyWebsocketApi,
            { System.currentTimeMillis() })
    }
    public val gameCredentialsRepository: GameCredentialsRepository by lazy {
        GameCredentialsRepositoryImpl(gameCredentialsDao)
    }
    public val usernameRepository: UsernameRepository by lazy {
        UsernameRepositoryImpl(usernameDao)
    }
}