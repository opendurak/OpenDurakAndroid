package com.opendurak.opendurakandroid.data.repository

import android.util.Log
import com.opendurak.opendurakandroid.data.api.GameWebsocketApi
import com.opendurak.opendurakandroid.data.api.LobbiesApi
import com.opendurak.opendurakandroid.data.api.LobbyApi
import com.opendurak.opendurakandroid.data.api.LobbyWebsocketApi
import com.opendurak.opendurakandroid.domain.model.GameCredentials
import com.opendurak.opendurakandroid.domain.repository.LobbiesRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.retry

internal class LobbiesRepositoryImpl(
    private val lobbiesApi: LobbiesApi,
    private val lobbyApi: LobbyApi,
    private val lobbyWebsocketApi: LobbyWebsocketApi,
    private val getCurrentMs: () -> Long,
) : LobbiesRepository {
    override suspend fun getLobbies() = lobbiesApi.getLobbies()

    override suspend fun listenToLobby(gameCredentials: GameCredentials): Flow<List<String>> {
        var lastRetryAtMs = 0L
        return lobbyWebsocketApi.getLobbyFlow(
            gameCredentials.userId,
            gameCredentials.lobbyId
        ).retry {
            Log.d(GameWebsocketApi::class.simpleName,
                "Stopped collection because ${it::class.simpleName}")
            val now = getCurrentMs()
            val recentlyRestarted = lastRetryAtMs + 30_000L > now
            if (recentlyRestarted) delay(5000) else delay(1000)
            lastRetryAtMs = now
            true
        }
    }

    override suspend fun createLobby() = lobbiesApi.createLobby()

    override suspend fun joinLobby(userName: String, lobbyId: String) =
        lobbyApi.join(lobbyId, userName)

}