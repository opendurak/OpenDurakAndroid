package com.opendurak.opendurakandroid.data.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.opendurak.opendurakapi.GameState
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.callbackFlow
import okhttp3.*
import okio.ByteString
import java.net.URI

internal class GameWebsocketApi(
    private val baseUrl: String,
    private val objectMapper: ObjectMapper,
    private val okHttpClient: OkHttpClient,
) {
    suspend fun getGameStateFlow(userId: String, lobbyId: String) =
        callbackFlow {
            val webSocket = okHttpClient.newWebSocket(
                Request.Builder()
                    .url(
                        URI.create(baseUrl)
                            .resolve("/lobby/$lobbyId/game/listenGameState?userId=$userId")
                            .normalize()
                            .toURL()
                    )
                    .build(),
                object : WebSocketListener() {

                    override fun onOpen(webSocket: WebSocket, response: Response) {}

                    override fun onMessage(webSocket: WebSocket, text: String) {
                        val gameState: GameState? =
                            objectMapper.readValue(text, GameState::class.java)
                        trySendBlocking(gameState)
                    }

                    override fun onMessage(webSocket: WebSocket, bytes: ByteString) {}

                    override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {}

                    override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                        this@callbackFlow.close()
                    }

                    override fun onFailure(
                        webSocket: WebSocket,
                        t: Throwable,
                        response: Response?,
                    ) {
                        this@callbackFlow.close(t)
                    }
                }
            )

            awaitClose {
                webSocket.close(1000, "")
            }
        }
}