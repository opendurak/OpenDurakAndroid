package com.opendurak.opendurakandroid

import android.app.Application
import com.opendurak.opendurakandroid.common.Injector

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Injector.application = this
    }
}