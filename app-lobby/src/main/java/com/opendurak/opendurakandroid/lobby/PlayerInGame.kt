package com.opendurak.opendurakandroid.lobby

data class PlayerInGame(
    val name: String,
    val numberOfCards: Int,
)
