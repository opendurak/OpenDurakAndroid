package com.opendurak.opendurakandroid.lobby

data class PlayerInRound(
    val name: String,
    val numberOfCards: Int,
    val isMe: Boolean,
    val gaveUp: Boolean,
)