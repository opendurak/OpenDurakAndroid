package com.opendurak.opendurakandroid.lobby

import com.opendurak.opendurakapi.Card
import com.opendurak.opendurakapi.GameState

sealed interface GameStateUi {
    data class Round(
        val attacker: PlayerInRound,
        val defender: PlayerInRound,
        val helper: PlayerInRound?,

        val battlefield: List<Pair<Card, Card?>>,
        val ownCards: List<Card>?,
        val playersInGame: List<PlayerInGame>,

        val spectators: Int,
        val cardsInStack: Int,
        val trumpCard: Card,
    ) : GameStateUi

    data class End(
        val durakName: String,
        val isDurakSelf: Boolean,
    ) : GameStateUi
}

fun GameState.mapToUi(lobbyUserCount: Int): GameStateUi =
    if (gameFinished) this.mapToEnd()
    else this.mapToRound(lobbyUserCount)

private fun GameState.mapToEnd(): GameStateUi.End {
    val selfPlayerHash = selfPlayer?.hash
    return if (selfPlayerHash == null) GameStateUi.End(players.first().username, false)
    else GameStateUi.End(players.first().username, players.first().hash == selfPlayerHash)
}

private fun GameState.mapToRound(lobbyUserCount: Int): GameStateUi.Round {
    val attackerPlayer = players.first { it.hash == attackerHash }
    val defenderPlayer = players.first { it.hash == defenderHash }
    val helper = if (helperHash != null) {
        val helperPlayer = players.first { it.hash == helperHash }
        PlayerInRound(
            helperPlayer.username,
            helperPlayer.cardCount,
            selfPlayer?.hash == helperHash,
            defenderGaveUp,
        )
    } else null

    return GameStateUi.Round(
        PlayerInRound(
            attackerPlayer.username,
            attackerPlayer.cardCount,
            selfPlayer?.hash == attackerHash,
            attackGaveUp,
        ),
        PlayerInRound(
            defenderPlayer.username,
            defenderPlayer.cardCount,
            selfPlayer?.hash == defenderHash,
            defenderGaveUp,
        ),
        helper,
        battlefield,
        selfPlayer?.cards,
        players.map { PlayerInGame(it.username, it.cardCount) },
        lobbyUserCount - players.size,
        stackCount,
        trump,
    )
}
