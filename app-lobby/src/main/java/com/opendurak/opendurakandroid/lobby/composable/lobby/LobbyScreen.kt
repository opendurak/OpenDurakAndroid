package com.opendurak.opendurakandroid.lobby.composable.lobby

import android.content.res.Configuration
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobby.R
import com.opendurak.opendurakandroid.lobby.composable.appbar.AppBar

@Composable
fun LobbyScreen(
    lobbyId: String,
    startGame: () -> Unit,
    userNames: List<String>,
    leaveLobby: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Scaffold(
        modifier = modifier,
        topBar = {
            AppBar(stringResource(R.string.lobby), leaveLobby, lobbyId)
        },
        floatingActionButton = {
            if (userNames.size > 1) {
                FloatingActionButton(onClick = { startGame() }) {
                    Icon(
                        imageVector = Icons.Default.PlayArrow,
                        contentDescription = stringResource(R.string.start_game)
                    )
                }
            }
        },
    ) {
        UsersList(userNames = userNames, modifier = Modifier.padding(it))
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DarkPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            LobbyScreen(
                "f69196a6-00e6-476a-b1da-4f77dba532ea",
                {},
                listOf("Peter", "Garen", "Siegfried"),
                {},
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            LobbyScreen(
                "f69196a6-00e6-476a-b1da-4f77dba532ea",
                {},
                listOf("Peter", "Garen"),
                {},
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreviewWithFewPlayers() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            LobbyScreen(
                "f69196a6-00e6-476a-b1da-4f77dba532ea",
                {},
                listOf("Peter"),
                {},
            )
        }
    }
}