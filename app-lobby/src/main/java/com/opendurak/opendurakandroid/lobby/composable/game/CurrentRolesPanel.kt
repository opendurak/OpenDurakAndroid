package com.opendurak.opendurakandroid.lobby.composable.game

import android.content.res.Configuration
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.ScaleFactor
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobby.PlayerInRound

private enum class Role { ATTACKER, HELPER, DEFENDER }

@Composable
fun CurrentRolesPanel(
    attacker: PlayerInRound,
    defender: PlayerInRound,
    helper: PlayerInRound?,
    modifier: Modifier = Modifier,
) {
    Column(modifier = modifier) {
        PlayerPanel(role = Role.ATTACKER, player = attacker)
        Row {
            Image(
                modifier = Modifier.weight(1f),
                imageVector = Icons.Default.ArrowRightAlt,
                colorFilter = ColorFilter.tint(MaterialTheme.colors.onBackground),
                contentDescription = "",
                contentScale = object : ContentScale {
                    override fun computeScaleFactor(srcSize: Size, dstSize: Size) =
                        ScaleFactor(dstSize.width / srcSize.width, 1f)
                })
            PlayerPanel(role = Role.DEFENDER, player = defender)
        }
        if (helper != null) PlayerPanel(role = Role.HELPER, player = helper)
    }
}

@Composable
private fun PlayerPanel(
    role: Role,
    player: PlayerInRound,
    modifier: Modifier = Modifier,
) {
    val border =
        if (player.isMe) modifier
            .border(BorderStroke(1.dp, MaterialTheme.colors.secondary))
            .padding(4.dp)
        else modifier
    Row(modifier = border) {
        when (role) {
            Role.ATTACKER -> Icon(Icons.Default.Bolt, contentDescription = "Attacker")
            Role.HELPER -> Icon(Icons.Default.LocalHospital, contentDescription = "Helper")
            Role.DEFENDER -> Icon(Icons.Default.Shield, contentDescription = "Defender")
        }
        Text(modifier = Modifier.padding(start = 8.dp), text = player.name)
        Text(
            modifier = Modifier
                .padding(start = 8.dp)
                .border(BorderStroke(1.dp, MaterialTheme.colors.onBackground))
                .padding(start = 2.dp, end = 2.dp),
            text = player.numberOfCards.toString()
        )
        if (player.gaveUp) Icon(
            Icons.Default.Check,
            tint = Color.Green,
            contentDescription = "Done"
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background, modifier = Modifier.fillMaxWidth()) {
            CurrentRolesPanel(
                attacker = PlayerInRound("Alice", 3, true, false),
                defender = PlayerInRound("Bob", 5, false, false),
                helper = PlayerInRound("Charles", 2, false, true),
            )
        }
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DarkPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background, modifier = Modifier.fillMaxWidth()) {
            CurrentRolesPanel(
                attacker = PlayerInRound("Alice", 3, true, false),
                defender = PlayerInRound("Bob", 5, false, false),
                helper = PlayerInRound("Charles", 2, false, true),
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun AttackerPanelPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            PlayerPanel(
                Role.ATTACKER,
                PlayerInRound("Alice", 3, true, false),
            )
        }
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DarkAttackerPanelPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            PlayerPanel(
                Role.ATTACKER,
                PlayerInRound("Alice", 3, true, false),
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DefenderPanelPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            PlayerPanel(
                Role.DEFENDER,
                PlayerInRound("Bob", 4, false, false),
            )
        }
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DarkDefenderPanelPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            PlayerPanel(
                Role.DEFENDER,
                PlayerInRound("Bob", 4, false, false),
            )
        }
    }
}


@Preview(showBackground = true)
@Composable
private fun HelperPanelPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            PlayerPanel(
                Role.HELPER,
                PlayerInRound("Charles", 4, false, true),
            )
        }
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DarkHelperPanelPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            PlayerPanel(
                Role.HELPER,
                PlayerInRound("Charles", 4, false, true),
            )
        }
    }
}
