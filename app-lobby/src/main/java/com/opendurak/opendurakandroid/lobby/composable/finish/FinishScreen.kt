package com.opendurak.opendurakandroid.lobby.composable.finish

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobby.GameStateUi
import com.opendurak.opendurakandroid.lobby.R
import com.opendurak.opendurakandroid.lobby.composable.appbar.AppBar
import java.util.*

@Composable
fun FinishScreen(
    lobbyId: String,
    userNames: List<String>,
    leaveLobby: () -> Unit,
    gameState: GameStateUi.End,
    startGame: () -> Unit,
    modifier: Modifier = Modifier,
) {
    Scaffold(
        topBar = {
            AppBar(stringResource(R.string.game_end), leaveLobby, lobbyId)
        },
    ) {
        Column(
            modifier = modifier
                .fillMaxSize()
                .padding(it)
                .background(
                    color = if (gameState.isDurakSelf) colorResource(id = R.color.red_200)
                    else colorResource(id = R.color.green_200)
                ),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center,
        ) {
            Box(
                modifier = Modifier.padding(24.dp),
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_cloud),
                    contentDescription = null
                )
                Column(
                    modifier = Modifier.matchParentSize(),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center,
                ) {
                    if (gameState.isDurakSelf) {
                        Text(stringResource(R.string.you), fontSize = 40.sp)
                        Text(stringResource(R.string.are_durak), fontSize = 20.sp)
                    } else {
                        Text(gameState.durakName, fontSize = 40.sp)
                        Text(stringResource(R.string.is_the_durak), fontSize = 20.sp)
                    }
                }
            }
            Button(
                modifier = Modifier.padding(top = 80.dp),
                onClick = startGame,
            ) {
                Text(text = stringResource(id = R.string.new_game))
                Icon(
                    modifier = Modifier.padding(start = 8.dp),
                    imageVector = Icons.Default.PlayArrow,
                    contentDescription = stringResource(R.string.search),
                )
            }
        }
    }
}

@Preview
@Composable
private fun FinishScreenPreviewLoose() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            FinishScreen(
                lobbyId = UUID.randomUUID().toString(),
                userNames = listOf("Tom", "Cathy", "Mick"),
                leaveLobby = {},
                gameState = GameStateUi.End("Maik", isDurakSelf = true),
                startGame = {},
            )
        }
    }
}

@Preview
@Composable
private fun FinishScreenPreviewWin() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            FinishScreen(
                lobbyId = UUID.randomUUID().toString(),
                userNames = listOf("Tom", "Cathy", "Mick"),
                leaveLobby = {},
                gameState = GameStateUi.End("Maik", isDurakSelf = false),
                startGame = {},
            )
        }
    }
}