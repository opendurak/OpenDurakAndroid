package com.opendurak.opendurakandroid.lobby

import androidx.lifecycle.*
import com.opendurak.opendurakandroid.common.SingleLiveEvent
import com.opendurak.opendurakandroid.domain.interactor.*
import com.opendurak.opendurakapi.Card
import com.opendurak.opendurakapi.DefendRequest
import com.opendurak.opendurakapi.GameState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class LobbyViewModel(
    private val connectLobbyUseCase: ConnectLobbyUseCase,
    private val startGameUseCase: StartGameUseCase,
    private val newLobbyId: String?,
    private val defendUseCase: DefendUseCase,
    private val giveUpUseCase: GiveUpUseCase,
    private val attackUseCase: AttackUseCase,
    private val helpUseCase: HelpUseCase,
) : ViewModel() {
    private val _users = MediatorLiveData<List<String>>()
    val users: LiveData<List<String>> = _users

    private val _gameState = MediatorLiveData<GameStateUi?>()
    val gameState: LiveData<GameStateUi?> = _gameState

    private val _lobbyId = MediatorLiveData<String>()
    val lobbyId: LiveData<String> = _lobbyId

    private val _leaveReason = MediatorLiveData<LeaveReason>()
    val leaveReason: LiveData<LeaveReason> = _leaveReason

    private val _userActionError = SingleLiveEvent<Unit>()
    val userActionError: LiveData<Unit> = _userActionError

    init {
        viewModelScope.launch {
            if (newLobbyId != null) {
                val (usersFlow, gameStateFlow) =
                    connectLobbyUseCase.joinAndConnect(newLobbyId)
                initializeAfterConnection(usersFlow, gameStateFlow, newLobbyId)
            } else {
                val lobbyId = connectLobbyUseCase.getJoinedLobbyId()
                if (lobbyId != null) {
                    val (usersFlow, gameStateFlow) = connectLobbyUseCase.connect()
                    initializeAfterConnection(usersFlow, gameStateFlow, lobbyId)
                } else {
                    _leaveReason.postValue(LeaveReason.NoLobby)
                }
            }
        }
    }

    private fun initializeAfterConnection(
        usersFlow: Flow<List<String>>,
        gameStateFlow: Flow<GameState?>,
        lobbyId: String,
    ) {
        _lobbyId.postValue(lobbyId)

        val usersLiveData = usersFlow.asLiveData()
        _users.addSource(usersLiveData) { _users.value = it }

        val gameStateLiveData = gameStateFlow.asLiveData()
        _gameState.addSource(gameStateLiveData) {
            _gameState.value = it?.mapToUi(it.players.size)
        }
    }

    fun startGame() {
        viewModelScope.launch {
            startGameUseCase.execute()
        }
    }

    fun leaveLobby() {
        viewModelScope.launch {
            connectLobbyUseCase.leaveLobby()
            _leaveReason.postValue(LeaveReason.LobbyLeft)
        }
    }

    fun defendCard(card: Card, cardToDefend: Card) {
        viewModelScope.launch {
            try {
                defendUseCase.execute(DefendRequest(card, cardToDefend))
            } catch (e: Exception) {
                _userActionError.postValue(Unit)
            }
        }
    }

    fun giveUp() {
        viewModelScope.launch {
            try {
                giveUpUseCase.execute()
            } catch (e: Exception) {
                _userActionError.postValue(Unit)
            }
        }
    }

    fun helpWithCard(card: Card) {
        viewModelScope.launch {
            try {
                helpUseCase.execute(card)
            } catch (e: Exception) {
                _userActionError.postValue(Unit)
            }
        }
    }

    fun attackWithCard(card: Card) {
        viewModelScope.launch {
            try {
                attackUseCase.execute(card)
            } catch (e: Exception) {
                _userActionError.postValue(Unit)
            }
        }
    }

    sealed interface LeaveReason {
        object NoLobby : LeaveReason
        object LobbyLeft : LeaveReason
    }
}