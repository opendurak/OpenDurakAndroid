package com.opendurak.opendurakandroid.lobby

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.opendurak.opendurakandroid.common.Injector

class LobbyViewModelFactory(
    private val lobbyId: String?,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return LobbyViewModel(
            Injector.connectLobbyUseCase,
            Injector.startGameUseCase,
            lobbyId,
            Injector.defendUseCase,
            Injector.giveUpUseCase,
            Injector.attackUseCase,
            Injector.helpUseCase,
        ) as T
    }
}