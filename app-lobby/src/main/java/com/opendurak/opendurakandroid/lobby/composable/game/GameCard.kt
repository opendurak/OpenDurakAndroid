package com.opendurak.opendurakandroid.lobby.composable.game

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.opendurak.opendurakapi.Card

@Composable
fun GameCard(
    card: Card,
    modifier: Modifier = Modifier,
) {
    val suit = card.suit.getLocalizedString()
    val value = card.value.getShortLocalizedString()

    Box(
        modifier
            .width((8 * 8).dp)
            .aspectRatio(6f / 9f)
            .background(MaterialTheme.colors.background)
            .border(BorderStroke(1.dp, MaterialTheme.colors.onBackground), RoundedCornerShape(4.dp))
    ) {
        Column(
            Modifier.padding(start = 4.dp, top = 4.dp),
        ) {
            Text(modifier = Modifier.align(Alignment.CenterHorizontally), text = value)
            Text(modifier = Modifier.align(Alignment.CenterHorizontally), text = suit)
        }
    }
}