package com.opendurak.opendurakandroid.lobby.composable.game

import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.opendurak.opendurakandroid.lobby.R
import com.opendurak.opendurakapi.Card

fun Card.Suit.getLocalizedString() = when (this) {
    Card.Suit.HEART -> "\u2665"
    Card.Suit.DIAMOND -> "\u2666"
    Card.Suit.CLUB -> "\u2663"
    Card.Suit.SPADE -> "\u2660"
}

@Composable
fun Card.Value.getShortLocalizedString() = when (this) {
    Card.Value.JACK -> stringResource(id = R.string.jack_short)
    Card.Value.QUEEN -> stringResource(id = R.string.queen_short)
    Card.Value.KING -> stringResource(id = R.string.king_short)
    Card.Value.ACE -> stringResource(id = R.string.ace_short)
    else -> this.weight.toString()
}

@Composable
fun Card.Value.getLongLocalizedString() = when (this) {
    Card.Value.JACK -> stringResource(id = R.string.jack)
    Card.Value.QUEEN -> stringResource(id = R.string.queen)
    Card.Value.KING -> stringResource(id = R.string.king)
    Card.Value.ACE -> stringResource(id = R.string.ace)
    else -> this.weight.toString()
}