package com.opendurak.opendurakandroid.lobby.composable.game

import android.content.res.Configuration
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.StackedLineChart
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobby.GameStateUi
import com.opendurak.opendurakandroid.lobby.PlayerInGame
import com.opendurak.opendurakandroid.lobby.PlayerInRound
import com.opendurak.opendurakandroid.lobby.R
import com.opendurak.opendurakandroid.lobby.composable.appbar.AppBar
import com.opendurak.opendurakapi.Card

@Composable
fun GameScreen(
    lobbyId: String,
    leaveLobby: () -> Unit,

    gameState: GameStateUi.Round,

    defendCard: (Card, Card) -> Unit,
    check: () -> Unit,
    attackWithCard: (Card) -> Unit,
    helpWithCard: (Card) -> Unit,
    modifier: Modifier = Modifier,
) = with(gameState) {
    Scaffold(
        topBar = {
            AppBar(stringResource(R.string.game), leaveLobby, lobbyId)
        },
        modifier = modifier,
    ) {
        var selectedPlayerCard by remember { mutableStateOf<Card?>(null) }

        if (!defender.isMe) selectedPlayerCard = null

        Column(modifier = Modifier.padding(it)) {
            Row(Modifier.weight(1f)) {
                Column(
                    modifier = Modifier.weight(1f),
                ) {
                    // Roles
                    CurrentRolesPanel(
                        attacker,
                        defender, helper, Modifier.padding(16.dp),
                    )
                    // Battlefield
                    BattlefieldPanel(
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally,
                        stacks = battlefield,
                        defendCard = {
                            val currentSelectedPlayerCard = selectedPlayerCard
                            if (defender.isMe && currentSelectedPlayerCard != null)
                                defendCard(currentSelectedPlayerCard, it)
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f),
                        rowModifier = Modifier.fillMaxWidth(),
                    )
                }
                // ButtonPanel
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                ) {
                    LobbyPopup(
                        spectatorsCount = spectators, players = playersInGame,
                    )
                    Row {
                        Icon(
                            Icons.Default.StackedLineChart,
                            contentDescription = stringResource(id = R.string.cards_in_stack)
                        )
                        Text(cardsInStack.toString())
                    }
                    Text(trumpCard.value.getShortLocalizedString() + trumpCard.suit.getLocalizedString())

                    Spacer(modifier = Modifier.weight(1f))

                    IconButton(
                        onClick = { check() }
                    ) {
                        Icon(
                            tint = Color.Green,
                            imageVector = Icons.Filled.Check,
                            contentDescription = stringResource(id = R.string.check)
                        )
                    }
                }
            }

            // PlayerCards
            if (ownCards != null) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .horizontalScroll(rememberScrollState())
                        .padding(16.dp),
                    horizontalArrangement = Arrangement.SpaceEvenly,
                ) {
                    ownCards.forEach {
                        var cardModifier = Modifier
                            .padding(start = 4.dp, end = 4.dp)
                            .clickable {
                                when {
                                    attacker.isMe -> attackWithCard(it)
                                    helper?.isMe == true -> helpWithCard(it)
                                    defender.isMe -> selectedPlayerCard = it
                                }
                            }
                        val isSelected = it == selectedPlayerCard
                        if (isSelected) {
                            cardModifier = cardModifier
                                .border(BorderStroke(1.dp, MaterialTheme.colors.secondary))
                                .padding(4.dp)
                        }
                        GameCard(
                            card = it,
                            cardModifier
                        )
                    }
                }
            }
        }
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DarkPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            GameScreen(
                "asdasd", {},
                GameStateUi.Round(
                    PlayerInRound("Hans", gaveUp = false, isMe = false, numberOfCards = 3),
                    PlayerInRound("Hans", gaveUp = false, isMe = false, numberOfCards = 3),
                    null,
                    listOf(randomCard() to null, randomCard() to randomCard()),
                    listOf(randomCard()),
                    listOf(PlayerInGame("Hans", 4), PlayerInGame("Gerd", 5)),
                    2,
                    4,
                    Card(Card.Suit.DIAMOND, Card.Value.SIX),
                ),
                { _: Card, _: Card -> }, {}, {}, { },
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            GameScreen(
                "asdasd", {},
                GameStateUi.Round(
                    PlayerInRound("Hans", gaveUp = false, isMe = false, numberOfCards = 3),
                    PlayerInRound("Hans", gaveUp = false, isMe = false, numberOfCards = 3),
                    null,
                    listOf(randomCard() to null, randomCard() to randomCard()),
                    listOf(randomCard()),
                    listOf(PlayerInGame("Hans", 4), PlayerInGame("Gerd", 5)),
                    2,
                    4,
                    Card(Card.Suit.DIAMOND, Card.Value.SIX),
                ),
                { _: Card, _: Card -> }, {}, {}, {},
            )
        }
    }
}

fun randomCard() = Card(Card.Suit.values().random(), Card.Value.values().random())