package com.opendurak.opendurakandroid.lobby.composable.appbar

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Logout
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobby.R
import com.opendurak.opendurakandroid.lobby.composable.AutoSizeText

@Composable
fun AppBar(
    title: String,
    leave: () -> Unit,
    lobbyId: String,
    modifier: Modifier = Modifier,
) {
    TopAppBar(
        modifier = modifier,
        navigationIcon = {
            IconButton(onClick = leave) {
                Icon(
                    imageVector = Icons.Default.Logout,
                    contentDescription = stringResource(R.string.search)
                )
            }
        },
        title = {
            Column {
                Text(text = title)
                AutoSizeText(
                    modifier = Modifier
                        .fillMaxWidth()
                        .heightIn(max = 32.dp),
                    textAlign = TextAlign.Left,
                    maxLines = 1,
                    text = lobbyId,
                    contentAlignment = Alignment.CenterStart
                )
            }
        },
        elevation = 12.dp
    )
}


@Preview
@Composable
private fun SearchAppBarPreview() {
    OpenDurakAndroidTheme {
        Surface(
            color = MaterialTheme.colors.background
        ) {
            AppBar("Lobby", {}, "f69196a6-00e6-476a-b1da-4f77dba532ea")
        }
    }
}

@Preview
@Composable
private fun SearchAppBarDarkPreview() {
    OpenDurakAndroidTheme(darkTheme = true) {
        Surface(
            color = MaterialTheme.colors.background
        ) {
            AppBar("Lobby", {}, "f69196a6-00e6-476a-b1da-4f77dba532ea")
        }
    }
}
