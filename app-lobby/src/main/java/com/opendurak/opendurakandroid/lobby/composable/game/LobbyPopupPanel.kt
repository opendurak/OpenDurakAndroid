package com.opendurak.opendurakandroid.lobby.composable.game

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Group
import androidx.compose.material.icons.filled.Visibility
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobby.PlayerInGame
import com.opendurak.opendurakandroid.lobby.R

@Composable
fun LobbyPopup(
    spectatorsCount: Int,
    players: List<PlayerInGame>,
    modifier: Modifier = Modifier,
) {
    var expanded by remember { mutableStateOf(false) }

    IconButton(
        modifier = modifier,
        onClick = { expanded = true }
    ) {
        Icon(Icons.Filled.Group, contentDescription = stringResource(id = R.string.lobby))
        DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false }) {
            PopupContent(spectatorsCount, players)
        }
    }
}

@Composable
private fun PopupContent(
    spectatorsCount: Int,
    players: List<PlayerInGame>,
    modifier: Modifier = Modifier,
) {
    Column(modifier.padding(8.dp)) {
        Row(Modifier.padding(bottom = 8.dp)) {
            Icon(
                Icons.Default.Visibility,
                contentDescription = stringResource(id = R.string.spectators)
            )
            Text(
                modifier = Modifier
                    .padding(start = 8.dp)
                    .align(CenterVertically), text = spectatorsCount.toString()
            )
        }
        players.forEach {
            Row(Modifier.padding(top = 8.dp)) {
                Text(text = it.name)
                Text(
                    modifier = Modifier
                        .padding(start = 8.dp)
                        .border(BorderStroke(1.dp, MaterialTheme.colors.onBackground))
                        .padding(start = 2.dp, end = 2.dp),
                    text = it.numberOfCards.toString()
                )
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun DarkPreview2() {
    OpenDurakAndroidTheme {
        Surface(
            color = MaterialTheme.colors.background
        ) {
            PopupContent(
                spectatorsCount = 5,
                players = listOf(
                    PlayerInGame("Hans", 4),
                    PlayerInGame("Gustav", 6),
                    PlayerInGame("Otto", 3),
                )
            )
        }
    }
}