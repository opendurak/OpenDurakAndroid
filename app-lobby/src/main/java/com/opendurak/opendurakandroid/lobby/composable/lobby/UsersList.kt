package com.opendurak.opendurakandroid.lobby.composable.lobby

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun UsersList(
    userNames: List<String>,
    modifier: Modifier = Modifier,
) {
    LazyColumn(
        modifier = modifier
            .fillMaxWidth()
            .fillMaxHeight(),
    ) {
        items(userNames.size, itemContent = {
            UserListItem(userNames[it])
        })
    }
}