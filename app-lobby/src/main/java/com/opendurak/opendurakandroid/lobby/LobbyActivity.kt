package com.opendurak.opendurakandroid.lobby

import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import com.opendurak.opendurakandroid.common.Consts.EXTRA_LOBBY_ID
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakandroid.lobby.composable.finish.FinishScreen
import com.opendurak.opendurakandroid.lobby.composable.game.GameScreen
import com.opendurak.opendurakandroid.lobby.composable.lobby.LobbyScreen
import com.opendurak.opendurakapi.Card

class LobbyActivity : AppCompatActivity() {
    private val lobbyViewModel by viewModels<LobbyViewModel> {
        LobbyViewModelFactory(intent.getStringExtra(EXTRA_LOBBY_ID))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lobbyViewModel.leaveReason.observe(this) {
            when (it) {
                LobbyViewModel.LeaveReason.NoLobby -> {
                    Toast.makeText(this, getString(R.string.no_lobby_provided), Toast.LENGTH_SHORT)
                        .show()
                    finish()
                }
                LobbyViewModel.LeaveReason.LobbyLeft -> finish()
            }
        }
        lobbyViewModel.userActionError.observe(this) {
            Toast.makeText(this, "Action failed", Toast.LENGTH_SHORT).show()
        }

        setContent {
            val lobbyId by lobbyViewModel.lobbyId.observeAsState("")
            val gameState by lobbyViewModel.gameState.observeAsState()
            val userNames by lobbyViewModel.users.observeAsState(initial = emptyList())

            OpenDurakAndroidTheme {
                Surface(color = MaterialTheme.colors.background) {
                    val currentGameState = gameState
                    when (currentGameState) {
                        null ->
                            LobbyScreen(
                                lobbyId,
                                { lobbyViewModel.startGame() },
                                userNames,
                                { lobbyViewModel.leaveLobby() },
                            )
                        is GameStateUi.Round -> GameScreen(
                            lobbyId,
                            { lobbyViewModel.leaveLobby() },
                            currentGameState,
                            { card: Card, cardToDefend: Card ->
                                lobbyViewModel.defendCard(card, cardToDefend)
                            },
                            { lobbyViewModel.giveUp() },
                            { lobbyViewModel.attackWithCard(it) },
                            { lobbyViewModel.helpWithCard(it) },
                        )
                        is GameStateUi.End -> {
                            FinishScreen(
                                lobbyId,
                                userNames,
                                { lobbyViewModel.leaveLobby() },
                                currentGameState,
                                { lobbyViewModel.startGame() },
                            )
                        }
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        lobbyViewModel.leaveLobby()
    }
}
