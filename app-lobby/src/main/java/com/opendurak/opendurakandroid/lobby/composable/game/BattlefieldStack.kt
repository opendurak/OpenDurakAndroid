package com.opendurak.opendurakandroid.lobby.composable.game

import android.content.res.Configuration
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.opendurak.opendurakandroid.common.compose.theme.OpenDurakAndroidTheme
import com.opendurak.opendurakapi.Card

@Composable
fun BattlefieldPanel(
    verticalArrangement: Arrangement.Vertical = Arrangement.Top,
    horizontalAlignment: Alignment.Horizontal = Alignment.Start,
    stacks: List<Pair<Card, Card?>>,
    defendCard: (Card) -> Unit,
    modifier: Modifier = Modifier,
    rowModifier: Modifier = Modifier,
    stackModifier: Modifier = Modifier,
) {
    Column(modifier = modifier, verticalArrangement, horizontalAlignment) {
        repeat(2) { column ->
            val rowModifier = if (column == 1) rowModifier.padding(top = 16.dp) else rowModifier
            Row(rowModifier, horizontalArrangement = Arrangement.SpaceEvenly) {
                repeat(3) { row ->
                    val index = (column * 3) + row

                    val stack = stacks.getOrNull(index)
                    if (stack != null) BattlefieldStack(
                        attackingCard = stack.first,
                        defendingCard = stack.second,
                        defendCard,
                        stackModifier
                    )
                }
            }
        }
    }
}

@Composable
private fun BattlefieldStack(
    attackingCard: Card,
    defendingCard: Card?,
    defendCard: (Card) -> Unit,
    modifier: Modifier = Modifier,
) {
    val modifier =
        if (defendingCard == null) modifier.clickable { defendCard(attackingCard) } else modifier
    Box(
        modifier = modifier,
    ) {
        GameCard(card = attackingCard)
        if (defendingCard != null) GameCard(
            card = defendingCard,
            Modifier.padding(top = 16.dp, start = 24.dp)
        )
    }
}

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            BattlefieldPanel(
                stacks = listOf(
                    Pair(
                        Card(Card.Suit.DIAMOND, Card.Value.SEVEN),
                        Card(Card.Suit.DIAMOND, Card.Value.QUEEN),
                    ),
                    Pair(
                        Card(Card.Suit.DIAMOND, Card.Value.KING),
                        null,
                    ),
                    Pair(
                        Card(Card.Suit.CLUB, Card.Value.SEVEN),
                        Card(Card.Suit.CLUB, Card.Value.QUEEN),
                    ),
                    Pair(
                        Card(Card.Suit.SPADE, Card.Value.TEN),
                        Card(Card.Suit.SPADE, Card.Value.JACK),
                    ),
                ),
                defendCard = {},
            )
        }
    }
}

@Preview(showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun DarkPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            BattlefieldPanel(
                stacks = listOf(
                    Pair(
                        Card(Card.Suit.DIAMOND, Card.Value.SEVEN),
                        Card(Card.Suit.DIAMOND, Card.Value.QUEEN),
                    ),
                    Pair(
                        Card(Card.Suit.DIAMOND, Card.Value.KING),
                        null,
                    ),
                    Pair(
                        Card(Card.Suit.CLUB, Card.Value.SEVEN),
                        Card(Card.Suit.CLUB, Card.Value.QUEEN),
                    ),
                    Pair(
                        Card(Card.Suit.SPADE, Card.Value.TEN),
                        Card(Card.Suit.SPADE, Card.Value.JACK),
                    ),
                ),
                defendCard = {},
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun StackPreview() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            BattlefieldStack(
                attackingCard = Card(Card.Suit.DIAMOND, Card.Value.SEVEN),
                defendingCard = Card(Card.Suit.DIAMOND, Card.Value.QUEEN),
                defendCard = {},
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
private fun StackPreview2() {
    OpenDurakAndroidTheme {
        Surface(color = MaterialTheme.colors.background) {
            BattlefieldStack(
                attackingCard = Card(Card.Suit.DIAMOND, Card.Value.SEVEN),
                defendingCard = null,
                defendCard = {},
            )
        }
    }
}
