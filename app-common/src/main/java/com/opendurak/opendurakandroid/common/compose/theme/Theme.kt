package com.opendurak.opendurakandroid.common.compose.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import com.opendurak.opendurakandroid.common.R

@Composable
public fun OpenDurakAndroidTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit,
) {
    val colors = if (darkTheme) getDarkColorPalette()
    else getLightColorPalette()

    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}

@Composable
private fun getLightColorPalette() = lightColors(
    primary = colorResource(id = R.color.blue_grey_500),
    primaryVariant = colorResource(id = R.color.blue_grey_700),
    secondary = colorResource(id = R.color.cyan_200),
)

@Composable
private fun getDarkColorPalette() = darkColors(
    primary = colorResource(id = R.color.blue_grey_200),
    primaryVariant = colorResource(id = R.color.blue_grey_700),
    secondary = colorResource(id = R.color.cyan_700),
    background = Color.Black,
)