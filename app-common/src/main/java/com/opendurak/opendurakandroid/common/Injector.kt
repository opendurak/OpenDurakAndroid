package com.opendurak.opendurakandroid.common

import android.app.Application
import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.opendurak.opendurakandroid.data.Injector
import com.opendurak.opendurakandroid.domain.interactor.*
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

public object Injector {
    public lateinit var application: Application

    private val context by lazy<Context> { application }

    // Data module

    private val objectMapper by lazy {
        ObjectMapper().registerKotlinModule()
    }
    private val okHttpClient by lazy {
        OkHttpClient().newBuilder()
            .addInterceptor(ChuckerInterceptor.Builder(context)
                .collector(ChuckerCollector(context))
                .maxContentLength(250000L)
                .redactHeaders(emptySet())
                .alwaysReadResponseBody(false)
                .build())
            .build()
    }
    private const val baseUrl = "https://opendurak.pascaldornfeld.de"
    private val retrofit by lazy {
        Retrofit
            .Builder()
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .build()
    }

    private val dataInjector by lazy {
        Injector(baseUrl, retrofit, okHttpClient, objectMapper, context)
    }

    // Domain module
    public val startGameUseCase: StartGameUseCase by lazy {
        StartGameUseCase(
            dataInjector.gameRepository, dataInjector.gameCredentialsRepository,
        )
    }
    public val attackUseCase: AttackUseCase by lazy {
        AttackUseCase(
            dataInjector.gameRepository, dataInjector.gameCredentialsRepository,
        )
    }
    public val helpUseCase: HelpUseCase by lazy {
        HelpUseCase(
            dataInjector.gameRepository, dataInjector.gameCredentialsRepository,
        )
    }
    public val defendUseCase: DefendUseCase by lazy {
        DefendUseCase(
            dataInjector.gameRepository, dataInjector.gameCredentialsRepository,
        )
    }
    public val giveUpUseCase: GiveUpUseCase by lazy {
        GiveUpUseCase(
            dataInjector.gameRepository, dataInjector.gameCredentialsRepository,
        )
    }
    public val createLobbyUseCase: CreateLobbyUseCase by lazy {
        CreateLobbyUseCase(dataInjector.lobbiesRepository)
    }
    public val connectLobbyUseCase: ConnectLobbyUseCase by lazy {
        ConnectLobbyUseCase(
            dataInjector.lobbiesRepository,
            dataInjector.gameRepository,
            dataInjector.gameCredentialsRepository,
            dataInjector.usernameRepository,
        )
    }
    public val getLobbiesUseCase: GetLobbiesUseCase by lazy {
        GetLobbiesUseCase(dataInjector.lobbiesRepository)
    }
    public val usernameUseCase: UsernameUseCase by lazy {
        UsernameUseCase(dataInjector.usernameRepository)
    }
}